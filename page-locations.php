<?php 

/*
 * Template Name: Locations Page
 */

get_header(); ?>

            <div id="content">

                <div id="inner-content" class="wrap clearfix">

                        <div id="main" class="eightcol clearfix" role="main">
                        
                            <article <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                            
                                <header class="article-header">

                                    <h1 class="page-title" itemprop="headline">Our Locations</h1>
            
                                </header> <!-- end article header -->

                                <section class="entry-content clearfix" itemprop="articleBody">

                            <?php
                            $thisreg = $wf->term(str_replace(" ", "", strtolower($_COOKIE["region"])), "region");
                            
                            $state_a = strtolower($_COOKIE["region"]);                          
                            if ($state_a == 'northern territory' || $state_a == 'victoria') {
                                
                                foreach ($thisreg->children(array('hide_empty'=>1,'order'=>'ASC')) as $reg) {
                                    if (!$reg->has_children()) {
                                        // only 2 tiers of regions
                                        echo "<h2 class='two a'>$reg->name</h2>";
                                        
                                        echo "<ul>";
            
                                        foreach ($reg->posts as $loc) {
                                            echo "<li>$loc->link</li>";
                                        }
                                        
                                        echo "</ul>";
                                        
                                    } else {
                                        // 3 tiers of regions
                                        echo "<h2 class='one b'>$reg->name</h2>";
                                        
                                        echo "<ul>";
                                    
                                        foreach ($reg->children as $regchild) {
                                            if ($regchild->count > 0) { // kh
                                                echo "<li>$regchild->name</li><ul>";
                                            
                                                foreach ($regchild->posts as $loc) {
                                                    echo "<li>$loc->link</li>";
                                                }
                                            
                                                echo "</ul>";
                                            } // end if kh
                                        }
                                        
                                        echo "</ul>";
                                    }
                                    
                                } //foreach 
                            
                                
                            } else {
                            
                                foreach ($thisreg->children(array('hide_empty'=>1,'orderby'=>'id','order'=>'ASC')) as $reg) {
                                    if (!$reg->has_children()) {
                                        // only 2 tiers of regions
                                        echo "<h2 class='two c'>$reg->name</h2>";
                                        
                                        echo "<ul>";
            
                                        foreach ($reg->posts as $loc) {
                                            echo "<li>$loc->link</li>";
                                        }
                                        
                                        echo "</ul>";
                                        
                                    } else {
                                        // 3 tiers of regions
                                        echo "<h2 class='one d'>$reg->name</h2>";
                                        
                                        echo "<ul>";
                                    
                                        foreach ($reg->children as $regchild) {
                                            if ($regchild->count > 0) { // kh
                                                echo "<li>$regchild->name</li><ul>";
                                                
                                                foreach ($regchild->posts as $loc) {
                                                    echo "<li>$loc->link</li>";
                                                }
                                                
                                                echo "</ul>";
                                             } // end if kh
                                        }
                                        
                                        echo "</ul>";
                                    }
                                    
                                } //foreach
                            
                            } ?>

                                </section> <!-- end article section -->

                            </article> <!-- end article -->


                        </div> <!-- end #main -->

                        <?php get_sidebar("service"); ?>

                </div> <!-- end #inner-content -->

            </div> <!-- end #content -->

<?php get_footer(); ?>
