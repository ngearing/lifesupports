<?php get_header(); ?>

<div id="content">
<div id="inner-content" class="wrap clearfix">
<div class="top-content">

    <div id="main" class="eightcol first clearfix" role="main">
        <?php
        if (have_posts()) :
            while (have_posts()) :
                the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="article-header row clearfix">
                    <div class="profilepic col-4">
                    <?php
                    if (has_post_thumbnail()) :
                        the_post_thumbnail('profile-pic', array( 'class' => 'counsellorpic' ));
                    endif; ?>
                    </div>
                    <div class="profile-right col-8">
                        <h1 class="entry-title single-title" itemprop="headline">
                            <?php the_title();?>
                        </h1>
                        
                        <div class="qualifications">
                            <?php echo $wf->the->counsellor_profile->counsellor_qualifications; ?>
                        </div>
                            
                        <div class="appts">
                            <p>APPOINTMENTS BY:</p>
                            <ul>
                            <?php
                            if ($wf->the->counsellor_options->appointments_by) :
                                foreach ($wf->the->counsellor_options->appointments_by->data->val as $key => $value) {
                                    if ($value == "Phone") {
                                        echo "<li class='$value' title='$value'><a href='https://www.lifesupports.com.au/phone-counselling/' title='Phone Counselling'>$value</a></li>";
                                    } else if ($value == "Skype") {
                                        echo "<li class='$value' title='$value'><a href='https://www.lifesupports.com.au/skype-counselling/' title='Skype Counselling'>$value</a></li>";
                                    } else {
                                        echo "<li class='$value' title='$value'>$value</li>";
                                    }
                                }
                            endif;
                            ?>
                            </ul>
                            <?php
                            if ($wf->the->counsellor_options->rebates->data->val != "") { ?>
                                <br /><br />
                                <p>REBATES AVAILABLE:</p>
                                <ul class="rebates-list">
                                <?php
                                foreach ($wf->the->counsellor_options->rebates->data->val as $key => $value) {
                                    if ($value == "Private Health Insurance") {
                                            echo "<li class='phi ".str_replace(" ", "", $value)."' title='$value'>$value</li>";
                                    } else {
                                        echo "<li class='".str_replace(" ", "", $value)."' title='$value'>$value</li>";
                                    }
                                }
                                ?>
                                </ul>
                            <?php
                            } else { ?>
                                <!-- <p>REBATES AVAILABLE</p>
                                    <ul class="rebates-list">

                                    </ul> -->
                            <?php
                            } ?>
                        </div><!-- /end appts -->
                    </div><!-- .profile-right -->
                    </header> <!-- end article header -->

                        <section class="entry-content clearfix" itemprop="articleBody">
                            <?php echo $wf->the->counsellor_profile->counsellor_bio; ?>
                            <?php
                            // get the locations that this counsellor works at, and some formatting
                            $locstring = "";
                            $locnum = 0;
                            foreach ($wf->the->incoming("post_type=location") as $location) {
                                $locstring .= $location->link.", ";
                                $locnum++;
                            }
                            
                            
                            if ($locnum > 1) {
                                $pos = strrpos($locstring, ",");
                                if ($pos !== false) {
                                    $locstring = substr_replace($locstring, "", $pos, strlen(","));
                                }
                                
                                $pos = strrpos($locstring, ",");
                                if ($pos !== false) {
                                    if ($locnum == 2) {
                                        $locstring = substr_replace($locstring, " and ", $pos, strlen(","));
                                    } else {
                                        $locstring = substr_replace($locstring, ", and ", $pos, strlen(","));
                                    }
                                }
                            } else {
                                $locstring = str_replace(",", "", $locstring);
                            }
                            ?>
                            
                            <p><?php echo $wf->the->title; ?> has appointments available in the following location<?php if ($locnum > 1){echo 's';}?>: <?php echo $locstring; ?></p>
                            
                        </section> <!-- end article section -->



                </article> <!-- end article -->

            <?php
            endwhile;
        else : ?>
                <article id="post-not-found" class="hentry clearfix">
                    <header class="article-header">
                        <h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
                    </section>
                    <footer class="article-footer">
                            <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
                    </footer>
                </article>
        <?php
        endif; ?>

            </div> <!-- end #main -->

            <?php get_sidebar("counsellor"); ?>
        
        </div>
    </div>

    <div id="second-content" class="clearfix" role="complementary">
        <?php $incposts = $wf->the->incoming("post_type=post"); ?>
        <div class="heading-wrap">
            <h2>Recent articles 
            <?php
            if (count($incposts)) {
                echo "by ".$wf->the->title;
            } else {
                echo "by our counsellors";
            } ?></h2>
        </div>
        <div class="bottom-content wrap">
            <div id="second" class="twelvecol clearfix" >
                
                <?php
                $numdone = 0;
                if (count($incposts)) {
                    foreach ($incposts as $the) {
                        if ($numdone==4) {
                            break;
                        }
                        outputposts($the);
                        $numdone++;
                    }
                } else {
                    foreach ($wf->type("post")->posts("orderby=post_date&posts_per_page=4") as $the) {
                        outputposts($the);
                    }
                }
                ?>

            </div>
        </div>


    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php
get_footer();
