jQuery( document ).ready(function() {
var error = true;
var postcode = "";

jQuery(function() {
var availableTags = [
"AUSTRALIAN NATIONAL UNIVERSITY	, 2000",
"BARTON	, 2210",
"HMAS CRESWELL	, 2540",
"JERVIS BAY	, 2540",
"BARTON	, 2600",
"CANBERRA	, 2600",
"DEAKIN	, 2600",
"DEAKIN WEST	, 2600",
"DUNTROON	, 2600",
"HARMAN	, 2600",
"HMAS HARMAN	, 2600",
"PARKES	, 2600",
"PARLIAMENT HOUSE	, 2600",
"RUSSELL	, 2600",
"YARRALUMLA	, 2600",
"ACTON	, 2601",
"BLACK MOUNTAIN	, 2601",
"CANBERRA	, 2601",
"AINSLIE	, 2602",
"DICKSON	, 2602",
"DOWNER	, 2602",
"HACKETT	, 2602",
"LYNEHAM	, 2602",
"O'CONNOR	, 2602",
"WATSON	, 2602",
"FORREST	, 2603",
"GRIFFITH	, 2603",
"MANUKA	, 2603",
"RED HILL	, 2603",
"CAUSEWAY	, 2604",
"KINGSTON	, 2604",
"NARRABUNDAH	, 2604",
"CURTIN	, 2605",
"GARRAN	, 2605",
"HUGHES	, 2605",
"CHIFLEY	, 2606",
"LYONS	, 2606",
"O'MALLEY	, 2606",
"PHILLIP	, 2606",
"SWINGER HILL	, 2606",
"WODEN	, 2606",
"FARRER	, 2607",
"ISAACS	, 2607",
"MAWSON	, 2607",
"PEARCE	, 2607",
"TORRENS	, 2607",
"CIVIC SQUARE	, 2608",
"CANBERRA INTERNATIONAL AIRPORT	, 2609",
"FYSHWICK	, 2609",
"MAJURA	, 2609",
"PIALLIGO	, 2609",
"SYMONSTON	, 2609",
"CHAPMAN	, 2611",
"DUFFY	, 2611",
"FISHER	, 2611",
"HOLDER	, 2611",
"MOUNT STROMLO	, 2611",
"PIERCES CREEK	, 2611",
"RIVETT	, 2611",
"STIRLING	, 2611",
"URIARRA	, 2611",
"URIARRA FOREST	, 2611",
"WARAMANGA	, 2611",
"WESTON	, 2611",
"WESTON CREEK	, 2611",
"BRADDON	, 2612",
"CAMPBELL	, 2612",
"REID	, 2612",
"TURNER	, 2612",
"ARANDA	, 2614",
"COOK	, 2614",
"HAWKER	, 2614",
"JAMISON CENTRE	, 2614",
"MACQUARIE	, 2614",
"PAGE	, 2614",
"SCULLIN	, 2614",
"WEETANGERA	, 2614",
"CHARNWOOD	, 2615",
"DUNLOP	, 2615",
"FLOREY	, 2615",
"FLYNN	, 2615",
"FRASER	, 2615",
"HIGGINS	, 2615",
"HOLT	, 2615",
"KIPPAX	, 2615",
"LATHAM	, 2615",
"MACGREGOR	, 2615",
"MELBA	, 2615",
"SPENCE	, 2615",
"BELCONNEN	, 2616",
"BELCONNEN	, 2617",
"BRUCE	, 2617",
"EVATT	, 2617",
"GIRALANG	, 2617",
"KALEEN	, 2617",
"LAWSON	, 2617",
"MCKELLAR	, 2617",
"UNIVERSITY OF CANBERRA	, 2617",
"HALL	, 2618",
"HUME	, 2620",
"KOWEN FOREST	, 2620",
"OAKS ESTATE	, 2620",
"THARWA	, 2620",
"TOP NAAS	, 2620",
"GREENWAY	, 2900",
"TUGGERANONG	, 2900",
"KAMBAH	, 2902",
"ERINDALE CENTRE	, 2903",
"OXLEY	, 2903",
"WANNIASSA	, 2903",
"FADDEN	, 2904",
"GOWRIE	, 2904",
"MACARTHUR	, 2904",
"MONASH	, 2904",
"BONYTHON	, 2905",
"CALWELL	, 2905",
"CHISHOLM	, 2905",
"GILMORE	, 2905",
"ISABELLA PLAINS	, 2905",
"RICHARDSON	, 2905",
"THEODORE	, 2905",
"BANKS	, 2906",
"CONDER	, 2906",
"GORDON	, 2906",
"CRACE	, 2911",
"MITCHELL	, 2911",
"GUNGAHLIN	, 2912",
"FRANKLIN	, 2913",
"GINNINDERRA VILLAGE	, 2913",
"NGUNNAWAL	, 2913",
"NICHOLLS	, 2913",
"PALMERSTON	, 2913",
"AMAROO	, 2914",
"BONNER	, 2914",
"FORDE	, 2914",
"HARRISON	, 2914"
];
jQuery( "#tags" ).autocomplete({
	minLength: 3,
	source: availableTags,
	sfeelect: function (event, ui) {
		$(this).val(ui.item ? ui.item : " ");
		$("#errorMessage").text('');
		error = false;
		// $("form.ui-widget").trigger('submit');
	},    
    change: function (event, ui) {
    	if (!ui.item) {
	    	postcode = $('input#tags').val(); // For display purposes only
	    	error = true; 
	    } else {    
		    error = false;
		}
	}
});


$( "form.ui-widget" ).submit(function( event ) {
	if (error == true)  {

		// Try find it in availableTags
		var inp = $("input#tags").val();

		if (inp.length > 0) {
			for (var i=0; i< availableTags.length; i++){
				if (availableTags[i].indexOf(inp) != -1) {
					$("input#tags").val(availableTags[i]);
					error = false;
					return;
				} 
			}
		}

		event.preventDefault();
		$("#errorMessage").text('There are no results for your search. Please check your input and choose a location from the list.');
	} else {
	}   
});
});
}); // end jQuery
