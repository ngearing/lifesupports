<?php
/*
 * Template Name: Counsellors Page
 */
get_header();
$region = $_COOKIE["region"]; ?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">

        <div id="main" class="eightcol clearfix" role="main">
            
            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                <header class="article-header">

                    <h1 class="page-title" itemprop="headline">Our Counsellors</h1>

                </header> <!-- end article header -->
            
                <section class="entry-content clearfix" itemprop="articleBody">
                        
                    <?php
                    $doneids = array();
                    
                    foreach ($wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=asc") as $location) {
                        // get all counsellors in this location
                        foreach ($location->location_options as $locn) {
                            $cslrs = $locn->location_counsellors;
                            $cslrs = explode(",", $cslrs);
                            //echo "<!-- ".$locn->title." -->";
                            
                            foreach ($cslrs as $cslr) {
                                // output the counsellor
                                if ($cslr!="") {
                                    $counsellor = $wf->post(str_replace(" ", "", $cslr), "counsellor");
                                    $bio = explode(".", $counsellor->counsellor_profile->counsellor_teaser);
                                    
                                    // Get beautiful counsellors
                                    $quality = get_post_meta($counsellor->ID, 'quality');
                                    if ($counsellor->id != "" && in_array('Best', $quality)) {
                                        if (!isset($doneids[$counsellor->ID])) {
                                            // pic
                                            $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                                            $counsellor_location = '';
                                            $incoming_locations = $counsellor->incoming("post_type=location");
                                            if (count($incoming_locations)) {
                                                $ilocstring = "";
                                                foreach ($incoming_locations as $iloc) {
                                                    $ilocstring .= $iloc->link . ", ";
                                                }
                                                $counsellor_location .= substr($ilocstring, 0, strlen($ilocstring)-2);
                                            }

                                            echo "<div class='counsellor-minibox row clearfix'><a class='col-2' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-10'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p class='clocs'>Appointments available in: $counsellor_location</p><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                            
                    $doneids = array();
                            
                    foreach ($wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=asc") as $location) {
                        // get all counsellors in this location
                        foreach ($location->location_options as $locn) {
                            $cslrs = $locn->location_counsellors;
                            $cslrs = explode(",", $cslrs);
                            //echo "<!-- ".$locn->title." -->";
                                    
                            foreach ($cslrs as $cslr) {
                                // output the counsellor
                                if ($cslr!="") {
                                    $counsellor = $wf->post(str_replace(" ", "", $cslr), "counsellor");
                                    $bio = explode(".", $counsellor->counsellor_profile->counsellor_teaser);
                                            
                                    // Normal folk
                                    $quality = get_post_meta($counsellor->ID, 'quality');
                                    if ($counsellor->id != "" && (empty($quality) || in_array('Normal', $quality))) {
                                        if (!isset($doneids[$counsellor->ID])) {
                                            // pic
                                            $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                                            $counsellor_location = '';
                                            $incoming_locations = $counsellor->incoming("post_type=location");
                                            if (count($incoming_locations)) {
                                                $ilocstring = "";
                                                foreach ($incoming_locations as $iloc) {
                                                    $ilocstring .= $iloc->link . ", ";
                                                }
                                                $counsellor_location .= substr($ilocstring, 0, strlen($ilocstring)-2);
                                            }

                                            echo "<div class='counsellor-minibox row clearfix'><a class='col-2' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-10'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p class='clocs'>Appointments available in: $counsellor_location</p><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                            
                    $doneids = array();
                    foreach ($wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=asc") as $location) {
                        // get all counsellors in this location
                        foreach ($location->location_options as $locn) {
                            $cslrs = $locn->location_counsellors;
                            $cslrs = explode(",", $cslrs);
                            echo "<!-- ".$locn->title." -->";
                            
                            foreach ($cslrs as $cslr) {
                                // output the counsellor
                                if ($cslr!="") {
                                    $counsellor = $wf->post(str_replace(" ", "", $cslr), "counsellor");
                                    $bio = explode(".", $counsellor->counsellor_profile->counsellor_teaser);
                                    
                                    // Mediocre folk
                                    $quality = get_post_meta($counsellor->ID, 'quality');
                                    if ($counsellor->id != "" && in_array('Mediocre', $quality)) {
                                        if (!isset($doneids[$counsellor->ID])) {
                                            // pic
                                            $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                                            $counsellor_location = '';
                                            $incoming_locations = $counsellor->incoming("post_type=location");
                                            if (count($incoming_locations)) {
                                                $ilocstring = "";
                                                foreach ($incoming_locations as $iloc) {
                                                    $ilocstring .= $iloc->link . ", ";
                                                }
                                                $counsellor_location .= substr($ilocstring, 0, strlen($ilocstring)-2);
                                            }

                                            echo "<div class='counsellor-minibox row clearfix'><a class='col-2' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-10'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p class='clocs'>Appointments available in: $counsellor_location</p><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $doneids = array();
                    foreach ($wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=asc") as $location) {
                        // get all counsellors in this location
                        foreach ($location->location_options as $locn) {
                            $cslrs = $locn->location_counsellors;
                            $cslrs = explode(",", $cslrs);
                            echo "<!-- ".$locn->title." -->";
                            
                            foreach ($cslrs as $cslr) {
                                // output the counsellor
                                if ($cslr!="") {
                                    $counsellor = $wf->post(str_replace(" ", "", $cslr), "counsellor");
                                    $bio = explode(".", $counsellor->counsellor_profile->counsellor_teaser);
                                    
                                    // Ugly folk
                                    $quality = get_post_meta($counsellor->ID, 'quality');
                                    if ($counsellor->id != "" && in_array('Terrible', $quality)) {
                                        if (!isset($doneids[$counsellor->ID])) {
                                            // pic
                                            $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                                            $counsellor_location = '';
                                            $incoming_locations = $counsellor->incoming("post_type=location");
                                            if (count($incoming_locations)) {
                                                $ilocstring = "";
                                                foreach ($incoming_locations as $iloc) {
                                                    $ilocstring .= $iloc->link . ", ";
                                                }
                                                $counsellor_location .= substr($ilocstring, 0, strlen($ilocstring)-2);
                                            }

                                            echo "<div class='counsellor-minibox row clearfix'><a class='col-2' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-10'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p class='clocs'>Appointments available in: $counsellor_location</p><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";
                                        }
                                    }
                                }
                            }
                        }
                    } ?>

</section> <!-- end article section -->
</article> <!-- end article -->
</div> <!-- end #main -->

<?php get_sidebar("service"); ?>

</div> <!-- end #inner-content -->
</div> <!-- end #content -->

<?php get_footer(); ?>
