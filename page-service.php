<?php
/*
 * Template Name: Service Page
 */
get_header();
?>
<div id="content">

    <div id="inner-content" class="wrap clearfix">

            <div id="main" class="eightcol clearfix" role="main">

                <?php
                if (have_posts()) :
                    while (have_posts()) :
                        the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="article-header">
                    
                        <?php if (function_exists('yoast_breadcrumb')) {
                            yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                        } ?>

                        <h1 class="page-title" itemprop="headline">
                            <?php
                            if ($wf->the->page_options->overwrite_title != '') {
                                echo $wf->the->page_options->overwrite_title;
                            } else {
                                echo $wf->the->title;
                            }
                            ?>
                        </h1>

                    </header> <!-- end article header -->

                    <section class="entry-content clearfix" itemprop="articleBody">
                        <?php if (get_field('blue_box')) :?><div class="blue_box"><?php the_field('blue_box'); ?></div><?php endif; ?>
                        <?php the_content(); ?>
                    </section> <!-- end article section -->

                </article> <!-- end article -->

                <?php
                    endwhile;
                else : ?>
                <article id="post-not-found" class="hentry clearfix">
                    <header class="article-header">
                        <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
                    </section>
                    <footer class="article-footer">
                            <p><?php _e('This is the error message in the page.php template.', 'bonestheme'); ?></p>
                    </footer>
                </article>
                <?php
                endif; ?>

            </div> <!-- end #main -->

            <?php get_sidebar('service'); ?>
    </div>
            
    <div id="second-content" class="clearfix" role="complementary">
        <?php $incposts = $wf->category($wf->the->title); ?>
        <div class="heading-wrap">
            <h2>Recent articles 
            <?php
            if (count($incposts)) {
                echo 'related to '.$wf->the->title;
            } ?></h2>
        </div>
        <div class="bottom-content wrap">
            <div id="second" class="twelvecol clearfix" >
            <?php
            if (count($incposts)) {
                $num = 0;
                foreach ($incposts->post() as $the) {
                    ++$num;
                    if ($num < 5) {
                        outputposts($the);
                    } else {
                        break;
                    }
                }
            } else {
                foreach ($wf->type('post')->posts('orderby=post_date&posts_per_page=4') as $the) {
                    outputposts($the);
                }
            }
            ?>
            </div>
        </div>
    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php
get_footer();
