<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
    - head cleanup (remove rsd, uri links, junk css, ect)
    - enqueueing scripts & styles
    - theme support functions
    - custom menu output & fallbacks
    - related post function
    - page-navi function
    - removing <p> from around images
    - customizing the post excerpt
    - custom google+ integration
    - adding custom fields to user profiles
*/
require_once 'library/bones.php'; // if you remove this, bones will break

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes

add_image_size('profile-pic', 210, 305, true);
add_image_size('small-profile', 81, 116, true);

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars()
{
    register_sidebar(array(
        'id' => 'defaultsidebar',
        'name' => __('Default Sidebar', 'bonestheme'),
        'description' => __('The default sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'id' => 'homesidebar',
        'name' => __('Home Sidebar', 'bonestheme'),
        'description' => __('The home sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'id' => 'counsellorsidebar',
        'name' => __('Counsellor Sidebar', 'bonestheme'),
        'description' => __('The counsellor sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'id' => 'locationsidebar',
        'name' => __('Location Sidebar', 'bonestheme'),
        'description' => __('The location sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'id' => 'servicesidebar',
        'name' => __('Service Sidebar', 'bonestheme'),
        'description' => __('The service sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'id' => 'topbar',
        'name' => __('Top Bar', 'bonestheme'),
        'description' => __('The top bar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    /*
    to add more sidebars or widgetized areas, just copy
    and edit the above sidebar code. In order to call
    your new sidebar just use the following code:

    Just change the name to whatever your new
    sidebar's id is, for example:

    register_sidebar(array(
        'id' => 'sidebar2',
        'name' => __('Sidebar 2', 'bonestheme'),
        'description' => __('The second (secondary) sidebar.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    To call the sidebar in your template, you can just copy
    the sidebar.php file and rename it to your sidebar's name.
    So using the above example, it would be:
    sidebar-sidebar2.php

    */
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class();
    ?>>
        <article id="comment-<?php comment_ID();
    ?>" class="clearfix">
            <header class="comment-author vcard">
                <?php
                /*
                    this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
                    echo get_avatar($comment,$size='32',$default='<path_to_url>' );
                */
                ?>
                <!-- custom gravatar call -->
                <?php
                    // create variable
                    $bgauthemail = get_comment_author_email();
    ?>
                <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail);
    ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri();
    ?>/library/images/nothing.gif" />
                <!-- end custom gravatar call -->
                <?php printf(__('<cite class="fn">%s</cite>', 'bonestheme'), get_comment_author_link()) ?>
                <time datetime="<?php echo comment_time('Y-m-j');
    ?>"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php comment_time(__('F jS, Y', 'bonestheme'));
    ?> </a></time>
                <?php edit_comment_link(__('(Edit)', 'bonestheme'), '  ', '') ?>
            </header>
            <?php if ($comment->comment_approved == '0') : ?>
                <div class="alert alert-info">
                    <p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
                </div>
            <?php endif;
    ?>
            <section class="comment_content clearfix">
                <?php comment_text() ?>
            </section>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </article>
    <!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form)
{
    $form = '<form role="search" method="get" id="searchform" action="'.home_url('/').'" >
    <label class="screen-reader-text" for="s">'.__('Search for:', 'bonestheme').'</label>
    <input type="text" value="'.get_search_query().'" name="s" id="s" placeholder="'.esc_attr__('Search the Site...', 'bonestheme').'" />
    <input type="submit" id="searchsubmit" value="'.esc_attr__('Search').'" />
    </form>';

    return $form;
} // don't remove this bracket!

function add_service_rewrite()
{
    add_rewrite_tag('%service%', '([^&]+)');
    add_rewrite_tag('%loc%', '([^&]+)');
    add_rewrite_rule('^get-help/([^/]*)(?<!marriage-and-relationship-counselling)(?<!anger-management-counselling)(?<!drug-and-alcohol-counselling)/([^/]*)', 'index.php?pagename=get-help&service=$matches[1]&loc=$matches[2]', 'top');
    add_rewrite_rule('^help/([^/]*)/([^/]*)', 'index.php?pagename=get-help&service=$matches[1]&loc=$matches[2]', 'top');
}
add_action('init', 'add_service_rewrite');

class w_findhelp extends WP_Widget
{
    /** constructor -- name this the same as the class above */
    public function __construct()
    {
        parent::__construct(false, $name = 'LS: Find Help');
    }

    /** @see WP_Widget::widget -- do not rename this */
    public function widget($args, $instance)
    {
        global $wf, $counsellingoptionforfindhelp; // for if we're on a dynamic service page
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $message = $instance['message'];
        ?>
        <?php echo $before_widget;
        ?>
        <?php
        if ($title) {
            echo $before_title.$title.$after_title;
        }
        ?>
            <p><?php echo str_replace('[service]', $wf->the->title, $message); ?></p>
            <form action="/our-locations/find" method="get" class="clearfix ui-widget">
                <label for="tags"></label>
                <input type="submit" value="Go" /><input id="tags" type="text" name="postcode" placeholder="Suburb or Postcode" />
                <!-- <select name="service" id="findhelpselect" tabindex="1" class="pretty dk">
                <?php if (is_page('anger-management-counselling')) { ?>
                    <option value="Anger Management" selected>Anger Management</option>
                <?php } elseif (is_page('anxiety-and-depression-counselling')) { ?>
                    <option value="Anxiety and Depression" selected>Anxiety and Depression</option>
                <?php } elseif (is_page('child-and-adolescent-counselling')) { ?>
                    <option value="Child and Adolescent" selected>Child and Adolescent</option>
                <?php } elseif (is_page('drug-and-alcohol-counselling')) { ?>
                    <option value="Drug and Alcohol" selected>Drug and Alcohol</option>
                <?php } elseif (is_page('family-counselling')) { ?>
                    <option value="Family" selected>Family</option>
                <?php } elseif (is_page('general-counselling')) { ?>
                    <option value="General" selected>General</option>
                <?php } elseif (is_page('grief-counselling')) { ?>
                    <option value="Grief Counselling" selected>Grief Counselling</option>
                <?php } elseif (is_page('marriage-and-relationship-counselling')) { ?>
                    <option value="Marriage and Relationship" selected>Marriage and Relationship</option>
                <?php } elseif (is_page('parenting-counselling')) { ?>
                    <option value="Parenting" selected>Parenting</option>
                <?php } elseif (is_page('sexual-abuse-counselling')) { ?>
                    <option value="Sexual Abuse" selected>Sexual Abuse</option>
                <?php } elseif (is_page('sexuality-counselling')) { ?>
                    <option value="Sexuality" selected>Sexuality</option>
                <?php } else { ?>
                    <option value="" selected>- Area of Specialty -</option>
                <?php } ?>
                <?php echo $counsellingoptionforfindhelp; ?>
                </select> -->
            </form>
            <span id="errorMessage"></span>
            <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['message'] = strip_tags($new_instance['message']);

        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    public function form($instance)
    {
        $title = esc_attr($instance['title']);
        $message = esc_attr($instance['message']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title');
        ?>"><?php _e('Title:');
        ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title');
        ?>" name="<?php echo $this->get_field_name('title');
        ?>" type="text" value="<?php echo $title;
        ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('message');
        ?>"><?php _e('Message');
        ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('message');
        ?>" name="<?php echo $this->get_field_name('message');
        ?>" type="text" value="<?php echo $message;
        ?>" />
        </p>
        <?php
    }
} // class w_findhelp extends WP_Widget
add_action('widgets_init', create_function('', 'return register_widget("w_findhelp");'));

class w_spotlight extends WP_Widget
{
    /** constructor -- name this the same as the class above */
    public function __construct()
    {
        parent::__construct(false, $name = 'LS: Counsellor Spotlight');
    }

    /** @see WP_Widget::widget -- do not rename this */
    public function widget($args, $instance)
    {
        global $wf;
        extract($args);
        $region = 'all';
        if (isset($_COOKIE['region'])) {
            $region = $_COOKIE['region'];
        }

        $title = apply_filters('widget_title', $instance['title']);
        ?>
        <?php echo $before_widget; ?>
        <?php
        if ($title) {
            echo $before_title.$title.$after_title;
        }
        ?>
        <?php
        $num = 0;
        $retries = 0;
        $doneids = array();

        // for each location in set region
        foreach ($wf->type('location')->in_the(str_replace(' ', '', strtolower($region)), 'region', 'orderby=rand&post_status=publish') as $location) {
            // get all counsellors in this location
            $cslrs = $location->location_options->location_counsellors;
            $cslrs = explode(',', $cslrs);
            //$cslr = $cslrs[0];
            foreach ($cslrs as $cslr) {
                // output the counsellor
                $counsellor = $wf->post(''.trim($cslr), 'counsellor');

                $bio = explode('.', strip_tags($counsellor->counsellor_profile->counsellor_teaser)); // Change this to $counsellor->counsellor_profile->counsellor_teaser when Elina is ready
                $quality = get_post_meta($counsellor->ID, 'quality');

                if ($counsellor->exists() && !in_array('Terrible', $quality)) {
                    if (!isset($doneids[$counsellor->ID])) {
                        //echo "<div class='counsellor-minibox clearfix'><p class='ctitle'><a href='".$counsellor->permalink."'>".$counsellor->thumbnail."</a><strong>".$counsellor->link("title=$counsellor->title")."</strong>".$bio[0].".</p><p class='service-area'>Service Areas: ".$location."</p></div>";

                        $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                        echo "<div class='counsellor-minibox row clearfix'><a class='col-4' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-8'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";

                        $doneids[$counsellor->ID] = 'true';
                        ++$num;
                    } else {
                        ++$retries;
                    }
                }

                // we don't want to keep foreach'ing
                if ($num == 6) {
                    break;
                }
                if ($retries == 6) {
                    break;
                }
            }

            // we don't want to keep foreach'ing
            if ($num == 6) {
                break;
            }
            if ($retries == 6) {
                break;
            }
        }
        ?>
        <div class="readmore"><a href="<?php echo home_url('our-counsellors'); ?>">View all our counsellors &raquo;</a></div>
                            
        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    public function form($instance)
    {
        $title = esc_attr($instance['title']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title');
        ?>"><?php _e('Title:');
        ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title');
        ?>" name="<?php echo $this->get_field_name('title');
        ?>" type="text" value="<?php echo $title;
        ?>" />
        </p>
    <?php
    }
}
add_action('widgets_init', create_function('', 'return register_widget("w_spotlight");'));

class w_appointment extends WP_Widget
{
    /** constructor -- name this the same as the class above */
    public function __construct()
    {
        parent::__construct(false, $name = 'LS: Counsellor Appointment');
    }

    /** @see WP_Widget::widget -- do not rename this */
    public function widget($args, $instance)
    {
        global $wf, $locstring;
        extract($args);
        $region = $_COOKIE['region'];

        $title = apply_filters('widget_title', $instance['title']); ?>
        <?php echo $before_widget; ?>

        <?php
        if ($title) {
            echo $before_title.$title.$after_title;
        } ?>
        <p>
        <?php echo __('To make an appointment or enquiry, please call', 'lifesupports');
        
        // $locstring = str_replace(' and', ' or', $locstring);
        // if ($wf->the->post_type->label == 'Locations') {
        //     echo 'To make an appointment in '.$wf->the->title.', call us today.';
        // } else {
        //     echo 'To make an appointment or enquiry with '.$wf->the->title.' in '.$locstring.' call us today.';
        // }
        ?></p>
        <p class="sidebar-phone"><a style="text-decoration: none; color: inherit;" href="tel:1300 735 030"><?php echo do_shortcode('[phone]'); ?></a></p>
              
        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    public function form($instance)
    {
        $title = esc_attr($instance['title']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title');
        ?>"><?php _e('Title:');
        ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title');
        ?>" name="<?php echo $this->get_field_name('title');
        ?>" type="text" value="<?php echo $title;
        ?>" />
        </p>
    <?php
    }
}
add_action('widgets_init', create_function('', 'return register_widget("w_appointment");'));

class w_locationcounsellors extends WP_Widget
{
    /** constructor -- name this the same as the class above */
    public function __construct()
    {
        parent::__construct(false, $name = 'LS: Location Counsellors');
    }

    /** @see WP_Widget::widget -- do not rename this */
    public function widget($args, $instance)
    {
        global $wf;
        extract($args);
        $region = $_COOKIE['region'];

        $title = apply_filters('widget_title', $instance['title']);
        ?>
        <?php echo $before_widget; ?>
        <?php
        if ($title) {
            echo $before_title.$title.$after_title;
        }
        ?>
        <?php
        $doneids = array();
        foreach ($wf->location_options as $locn) {
            // get all counsellors in this location
            $cslrs = $locn->location_counsellors;
            $cslrs = explode(', ', $cslrs);

            foreach ($cslrs as $cslr) {
                // output the counsellor
                $counsellor = '';
                $counsellor = $wf->post(''.$cslr, 'counsellor');
                $bio = explode('.', $counsellor->counsellor_profile->counsellor_bio);
                if ($counsellor->id != '') { //} AND count($doneids) > 0) {
                    if (!isset($doneids[$counsellor->ID]) or !$doneids[$counsellor->ID]) {
                        //
                        $smallprofile = wp_get_attachment_image_src(get_post_thumbnail_id($counsellor->ID), 'small-profile', false);
                        echo "<div class='counsellor-minibox row clearfix'><a class='col-5' href='".$counsellor->permalink."'><img class='cimg' src='".$smallprofile[0]."'/></a><div class='col-7'><h4 class='ctitle'>".$counsellor->link("title=$counsellor->title")."</h4><p>".$bio[0]."... <a class='readmore' style='text-decoration:none;' href='".$counsellor->permalink."'>read more &raquo;</a></p></div></div>";

                        $doneids[$counsellor->ID] = 'true';
                    }
                }
            }
        }
        ?>              
        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    public function form($instance)
    {
        $title = esc_attr($instance['title']);
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title');
        ?>"><?php _e('Title:');
        ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title');
        ?>" name="<?php echo $this->get_field_name('title');
        ?>" type="text" value="<?php echo $title;
        ?>" />
        </p>
        <?php 
    }
}
add_action('widgets_init', create_function('', 'return register_widget("w_locationcounsellors");'));

/*class PhoneNumber {

static $instance = 0;

    function __construct($args = array()) {
      add_shortcode('phone', array($this, 'shortcode'));
    }

    static function shortcode($atts) {
      //this is the code of your shortcode
      return '<span id="numdiv_10833_'.self::$instance.'">(03) 9011 8469</span>';
      // you can increment your counter
      self::$instance++;

    }

}
$pnum = new PhoneNumber();*/

function phonenumber($attr)
{
    return '<span class="dnum">1300 735 030</span>';
}
add_shortcode('phone', 'phonenumber');
add_filter('widget_text', 'do_shortcode');

function locationsshortcode($attr)
{
    global $wf;
    $retstring = '';
    $locationsarray = array();
    foreach ($wf->type('location')->in_the($attr['region'], 'region', 'orderby=title&order=asc') as $location) {
        array_push($locationsarray, $location->link);
    }

    if (count($locationsarray) > 1) {
        $last = array_pop($locationsarray);

        foreach ($locationsarray as $locn) {
            $retstring .= $locn.', ';
        }

        $retstring .= 'and '.$last;
    } else {
        $retstring = $locationsarray[0];
    }

    return $retstring;
}
add_shortcode('locations', 'locationsshortcode');

// update the '51' to the ID of your form
/* add_filter('gform_pre_render_1', 'populate_posts');
function populate_posts($form){
    
    foreach($form['fields'] as &$field){

 

        
        if($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posts') === false)
            continue;
                // args
$args1 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'new south wales'
);
$args2 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'queensland'
);
$args3 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'northern territory'
);
$args4 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'australian capital territory'
);
$args5 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'south australia'
);
$args6 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'victoria'
);
$args7 = array(
    'numberposts' => -1,
    'post_type' => 'counsellor',
    'meta_key' => 'location',
    'meta_value' => 'western australia'
);
        
        // you can add additional parameters here to alter the posts that are retreieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        if ($_COOKIE['region']==="New South Wales"){
        $posts = get_posts($args1);
        } else if ($_COOKIE['region']==="Queensland") {
        $posts = get_posts($args2);
        } else if ($_COOKIE['region']==="Northern Territory") {
        $posts = get_posts($args3);
        } else if ($_COOKIE['region']==="Australian Capital Territory") {
        $posts = get_posts($args4);
        } else if ($_COOKIE['region']==="South Australia") {
        $posts = get_posts($args5);
        } else if ($_COOKIE['region']==="Victoria") {
        $posts = get_posts($args6);
        } else if ($_COOKIE['region']==="Western Australia") {
        $posts = get_posts($args7);
        }
        
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $choices = array(array('text' => 'Select a Counsellor', 'value' => ' '));
        
        foreach($posts as $post){
            $choices[] = array('text' => $post->post_title, 'value' => $post->post_title);
        }
        
        $field['choices'] = $choices;
        
    }
    
    return $form;
}

add_filter('gform_field_value_your_parameter', 'my_custom_population_function');
    function my_custom_population_function($value){
    global $wf;
        $region = $_COOKIE["region"];
       
       
                            $doneids = array();
                            
                            foreach ($wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=ASC") as $location) {
                                // get all counsellors in this location
                                foreach ($location->location_options as $locn) {
                                    $cslrs = $locn->location_counsellors;
                                    $cslrs = explode(",", $cslrs);
                                    echo "<!-- ".$locn->title." -->";
                                    
                                    foreach ($cslrs as $cslr) {
                                        // output the counsellor
                                        if ($cslr!="") {
    
                                            $counsellor = $wf->post(str_replace(" ","",$cslr), "counsellor");
                                            $bio = explode(".", $counsellor->counsellor_profile->counsellor_bio);
                                            if ($counsellor->id != "") {
                                                if (!$doneids[$counsellor->ID]) {
                                                    echo $counsellor->title;
                                                    
                                                    
                                                    $doneids[$counsellor->ID] = "true";
                                                }
                                            }
                                        }
                                    }
                                }
                                
                              
                            }
    
    } */

    // update the '51' to the ID of your form
add_filter('gform_pre_render_1', 'populate_posts');
function populate_posts($form)
{
    foreach ($form['fields'] as &$field) {
        if ($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posts') === false) {
            continue;
        }

        global $wf;
        $region = $_COOKIE['region'];
        $choices = array();
        $doneids = array();

        foreach ($wf->type('location')->in_the(str_replace(' ', '', strtolower($region)), 'region', 'orderby=ASC') as $location) {
            // get all counsellors in this location
                        foreach ($location->location_options as $locn) {
                            $cslrs = $locn->location_counsellors;

                            echo '<!-- '.$locn->title.' -->';
                            // update 'Select a Post' to whatever you'd like the instructive option to be

                            // output the counsellor

                            $counsellors = $wf->post(str_replace(' ', '', $cslrs), 'counsellor');

                            foreach ($cslrs as $counsellor):
                                if (!$doneids[$counsellor->ID]) {
                                    $choices[] = array('text' => $counsellor->title, 'value' => $counsellor->title);
                                }

                            endforeach;

                            $doneids[$counsellor->ID] = 'true';
                        }
        }

        asort($choices);
        array_unshift($choices, array('text' => 'Choose a Counsellor', 'value' => ' '));
        $field['choices'] = $choices;
    }

    return $form;
}

/* // update the '51' to the ID of your form
add_filter('gform_pre_render_1', 'populate_posts');
function populate_posts($form){
    
    foreach($form['fields'] as &$field){
        
        if($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posts') === false)
            continue;
        
        global $wf;
            $region = $_COOKIE["region"];
        // you can add additional parameters here to alter the posts that are retreieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $location = $wf->type("location")->in_the(str_replace(" ", "", strtolower($region)), "region", "orderby=ASC");
        $locn = $location->location_options;
        $cslrs = $locn->location_counsellors;
        
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $choices = array(array('text' => 'Select a Post', 'value' => ' '));
        
        foreach($cslrs as $post){
            $choices[] = array('text' => $post->title, 'value' => $post->title);
        }
        
        $field['choices'] = $choices;
        
    }
    
    return $form;
} */

function ggstyle_set_postthumbnail_size($size, $thumbnail_id, $post)
{
    if (!is_admin() || 'counsellor' != get_post_type($post)) {
        return $size;
    }

    $size = 'large';

    return $size;
}
add_filter('admin_post_thumbnail_size', 'ggstyle_set_postthumbnail_size', 10, 3);


/**
 * Add Template column to pages in admin
 */
function page_column_views($defaults)
{
    $defaults['page-layout'] = __('Template');
    return $defaults;
}
function page_custom_column_views($column_name)
{
    if ($column_name === 'page-layout') {
        $set_template = get_post_meta(get_the_ID(), '_wp_page_template', true);
        if ($set_template == 'default') {
            echo 'Default';
        }
        $templates = get_page_templates();
        ksort($templates);
        foreach (array_keys($templates) as $template) :
            if ($set_template == $templates[$template]) :
                echo $template;
            endif;
        endforeach;
    }
}
add_filter('manage_pages_columns', 'page_column_views');
add_action('manage_pages_custom_column', 'page_custom_column_views');


/**
 * This function outputs recent posts depending on the page.
 */
function outputposts($the)
{
    global $wf;
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

        <header class="article-header">
            <h3 class="page-title" itemprop="headline">
                <?php
                if ($the->page_options->overwrite_title != '') {
                    echo "<a href='".$the->permalink."' title='".$the->page_options->overwrite_title."' class='alignleft'>".$the->page_options->overwrite_title.'</a>';
                } else {
                    echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$the->title.'</a>';
                }
                ?>
            </h3>
        </header> <!-- end article header -->

        <section class="entry-content clearfix" itemprop="articleBody">
            <?php
            if ($the->has_featured_image()) {
                $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($the->ID), 'thumbnail', false);
                echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'><img alt='".$the->title."' height='150' width='150' src='".$thumbnail[0]."'></a>";
            } else {
                echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$wf->theme_image('default-thumb.jpg')->resize('w=150&h=150').'</a>';
            }

            echo strip_tags($the->excerpt);
            echo $the->link(array('text' => 'read more &raquo;', 'class' => 'readmore')); ?>
        </section> <!-- end article section -->
    </article> <!-- end article -->
<?php
}

