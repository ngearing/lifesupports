<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">

        <div id="main" class="eightcol first clearfix" role="main">

            <article id="post-not-found" class="hentry clearfix">

                <header class="article-header">

                    <h1><?php _e("Page or Article Not Found", "bonestheme"); ?></h1>

                </header> <!-- end article header -->

                <section class="entry-content">

                    <p><?php _e("Sorry, we couldn't locate the page/article you requested. Please try looking again.", "bonestheme"); ?></p>

                </section> <!-- end article section -->

                <section class="search">

                        <p><?php get_search_form(); ?></p>

                </section> <!-- end search section -->

                <footer class="article-footer">

                        <p></p>

                </footer> <!-- end article footer -->

            </article> <!-- end article -->

        </div> <!-- end #main -->

    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
