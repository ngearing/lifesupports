<?php
use GeoIp2\WebService\Client;

if (!strpos(home_url(), 'dev')) {
    require 'geoip2.phar';
}

if (!isset($_COOKIE['region']) || $_COOKIE['region'] == null) {
    $client = '';
    $record = '';
    if (!strpos(home_url(), 'dev')) {
        $client = new Client(93706, 'xZJ64h2djPxP');
        $record = $client->city($_SERVER['REMOTE_ADDR']);
    }

    if ($record && $record->mostSpecificSubdivision->name != 'Victoria') {
        setcookie('region', $record->mostSpecificSubdivision->name, time(), '/', 'lifesupportscounselling.com.au');
        $_COOKIE['region'] = $record->mostSpecificSubdivision->name;
    } else {
        setcookie('region', 'Victoria', time(), '/', 'lifesupportscounselling.com.au');
        $_COOKIE['region'] = 'Victoria';
    }
}

if (!isset($_COOKIE['getmore'])) {
    setcookie('getmore', 'Marriage and Relationship', time() + 3600 * 24 * 30, '/');
    $_COOKIE['getmore'] = 'Marriage and Relationship';
}

if (!isset($_COOKIE['firsttime'])) {
    setcookie('firsttime', 'false', time() + 3600 * 24 * 30, '/');
}

/**
 * DO TO:
 * Move this shit to functions
 */
$counselling = array(
    'Adolescent counselling'    => 'adolescent-counselling',
    'Anger management'          => 'anger-management-counselling',
    'Anxiety and depression'    => 'anxiety-and-depression-counselling',
    'Child counselling'         => 'child-counselling',
    'Drug and alcohol'          => 'drug-and-alcohol-counselling',
    'Family'                    => 'family-counselling',
    'General'                   => 'general-counselling',
    'Grief counselling'         => 'grief-counselling',
    'Marriage and relationship' => 'marriage-and-relationship-counselling',
    'Parenting'                 => 'parenting-counselling',
    'Sexual abuse'              => 'sexual-abuse-counselling',
    'Sexuality'                 => 'sexuality-counselling',
);

$counsellingoption = '';
foreach ($counselling as $key => $value) {
    $counsellingoption .= "<option value='$value'>$key</option>";
}

global $counsellingoptionforfindhelp;
$counsellingoptionforfindhelp = '';
foreach ($counselling as $key => $value) {
    $counsellingoptionforfindhelp .= "<option value='$key'>$key</option>";
}

// setup globals for the page-service-parent.php template
global $s_service, $s_location, $s_the;
?>

<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="author=" content="lifesupportscounselling.com.au"/>
<meta name="copyright=" content="lifesupportscounselling.com.au@2013"/>
<meta name="distribution" content="global"/>
<meta name="format-detection" content="telephone=no"/>
<meta name="language" content="English"/>
<meta name="msapplication-TileColor" content="#f01d4f">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
<meta name="revisit-after" content="3 Days"/>
<meta name="robots" content="index, follow"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--[if IE]>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<![endif]-->
<!-- or, set /favicon.ico for IE10 win -->

<title>
<?php
/**
 * DO TO:
 * Move this shit to functions
 */
// if we're using this template, we need to setup the page title properly
if (is_page_template('page-service-parent.php')) {
    $s_service = get_query_var('service');
    $s_location = get_query_var('loc');
    $s_the = $wf->type('page')->post($service);

    if ($s_the->page_options->overwrite_title != '') {
        echo $s_the->page_options->overwrite_title.' in '.ucwords(str_replace('-', ' ', $s_location)).' | Life Supports';
    } else {
        echo $s_the->title.' in '.ucwords(str_replace('-', ' ', $s_location)).' | Life Supports';
    }
} else {
    wp_title('');
}
?></title>

<?php wp_head(); ?>
  
<?php
/**
 * DO TO:
 * Move this shit to functions
 */
if (is_page(16)) { ?>
<!-- Contact Page -->
<!-- Facebook Conversion Code for Contact Form Submissions -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6024076602967', {'value':'0.00','currency':'AUD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6024076602967&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
<?php
} ?>

</head>
<body <?php body_class(); ?>>
<div id="container">

    <header class="header" role="banner">

        <div id="inner-header" class="wrap clearfix">

            <div class="site-branding h1 fivecol">
                <?php
                $site_logo = get_custom_logo();
                if ($site_logo) :
                    // Custom logo
                    if (is_front_page() || is_home()) :
                        echo "<h1 class='site-title'>$site_logo</h1>";
                    else :
                        echo "<p class='site-title'>$site_logo</p>";
                    endif;
                else :
                    // No custom logo
                    if (is_front_page() || is_home()) :
                        echo "<h1 class='site-title no-logo'><a href='".esc_url(home_url('/'))."' rel='home'>".get_bloginfo('name')."</a></h1>";
                    else :
                        echo "<p class='site-title no-logo'><a href='".esc_url(home_url('/'))."' rel='home'>".get_bloginfo('name')."</a></p>";
                    endif;
                endif; // $site_logo

                $description = get_bloginfo('description', 'display');
                if ($description || is_customize_preview()) : ?>
                    <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                <?php
                endif;
                ?>
            </div><!-- .site-branding -->

            <div id="flaghead" class="sevencol">
                <p class="phone">Phone <a href="tel:1300735030" id="numdiv_10833_0">1300 735 030</a>
            </div>
                    
        </div> <!-- end #inner-header -->
                
        <div id="navigation" class="clearfix main-navigation">
            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                <span class="menu-toggle-box">
                    <span class="menu-toggle-inner"></span>
                </span>
            </button>
            <div id="navigation-inner" class="clearfix">
                <nav role="navigation" class="twelvecol clearfix">
                    <?php bones_main_nav(); ?>
                </nav>
            </div>
        </div>
        
        <div id="head-banner">
            <div class="wrap clearfix">
                <div class="getmore">
                    <h4>Find counselling information about:</h4> 
                    <select name="getmoreinfo" id="getmoreselect" tabindex="1" class="divretty dk">
                        <option value="" selected>Please Select</option><?php echo $counsellingoption; ?>
                    </select>
                </div>
                <?php get_sidebar('topbar'); ?>
                <div class="arrowHold mobile-only"><a id="scroll" class="scrollTop" href="javascript:void(0)"><img id="scrollArrow" src="<?php bloginfo('template_url'); ?>/library/images/scrollArrow.png" /></a></div>
            </div>
        </div>

    </header> <!-- end header -->
