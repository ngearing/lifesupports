<?php get_header(); ?>
<div id="content">
    <div id="inner-content" class="wrap clearfix">
        <div class="top-content">

                <div id="main" class="eightcol clearfix" role="main">

                    <?php foreach ($wf->loop() as $the) : ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                        <header class="article-header">

                            <h1 class="page-title" itemprop="headline">
                                <?php
                                if ($the->page_options->overwrite_title != "") :
                                    echo $the->page_options->overwrite_title;
                                else :
                                    echo $the->title;
                                endif;
                                ?>
                            </h1>
    
                        </header> <!-- end article header -->

                        <section class="entry-content clearfix" itemprop="articleBody">
                            <?php echo $the->content ?>
                        </section> <!-- end article section -->

                    </article> <!-- end article -->

                    <?php endforeach; ?>

                </div> <!-- end #main -->
                

                <?php get_sidebar("home"); ?>
        </div>
    </div>
    <div id="second-content" class="clearfix" role="complementary">
        <div class="heading-wrap">
            <h2>Recent articles</h2>
        </div>
        <div class="bottom-content wrap">
            <div id="second" class="twelvecol clearfix" >
                
                <?php foreach ($wf->type("post")->posts("order=desc&orderby=date&posts_per_page=4") as $the) : ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                        <header class="article-header">

                            <h3 class="page-title" itemprop="headline">
                                <?php
                                if ($the->page_options->overwrite_title != "") {
                                    echo "<a href='".$the->permalink."' title='".$the->page_options->overwrite_title."' class='alignleft'>".$the->page_options->overwrite_title."</a>";
                                } else {
                                    echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$the->title."</a>";
                                }
                                ?>
                            </h3>
    
                        </header> <!-- end article header -->

                        <section class="entry-content clearfix" itemprop="articleBody">
                            <?php
                            $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($the->ID), 'thumbnail', false);
                            if ($the->has_featured_image()) {
                                echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'><img alt='".$the->title."' height='150' width='150' src='".$thumbnail[0]."'></a>";
                            } else {
                                echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$wf->theme_image("default-thumb.jpg")->resize("w=150&h=150")."</a>";
                            }

                            echo strip_tags($the->excerpt);
                            echo $the->link(array('text' => 'read more &raquo;', 'class' => 'readmore')); ?>
                        </section> <!-- end article section -->

                    </article> <!-- end article -->
                
                <?php endforeach; ?>
            </div>
        </div>

    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
