<?php
/*
 * Template Name: Find Nearby page
 */
include_once 'library/maps/GoogleMap.php';
include_once 'library/maps/JSMin.php';

$MAP_OBJECT = new GoogleMapAPI();
$MAP_OBJECT->_minify_js = isset($_REQUEST['min']) ? false : true;

get_header();

$service = (isset($_GET['service']) ? $_GET['service'] : false);
$postcode = (isset($_GET['postcode']) ? $_GET['postcode'] : false);
?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">

            <div id="main" class="eightcol clearfix" role="main">


                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                    <p class="head mobile-only">Search Results</p>

                    <header class="article-header">
                        <h1 class="page-title" itemprop="headline">Locations near <?php echo $postcode; ?> <!--that offer <?php //echo $service; ?>--></h1>
                    </header> <!-- end article header -->

                    <section class="entry-content clearfix" itemprop="articleBody">
                        <style type="text/css">#error {display: none;}</style>
                        <?php
                            // $strLocation = $_COOKIE["region"]." ".$_GET["postcode"];
                            $strLocation = $postcode;

                            $strStrippedLocation = preg_replace("/\s+/", ' ', trim(strip_tags($strLocation)));

                            $visitorLocation = $MAP_OBJECT->getGeoCode($strStrippedLocation);
                            echo '<!-- lookup lat('.$visitorLocation['lat'].') lon('.$visitorLocation['lon'].') -->';

                            $locations = array();

                            $myrows = $wpdb->get_results('SELECT post_id AS locid, ( 6371 * acos( cos( radians('.$visitorLocation['lat'].") ) * cos( radians( SUBSTRING_INDEX(meta_value,',',1) ) ) * cos( radians( SUBSTRING_INDEX(meta_value,',',-1) ) - radians(".$visitorLocation['lon'].') ) + sin( radians('.$visitorLocation['lat'].") ) * sin( radians( SUBSTRING_INDEX(meta_value,',',1) ) ) ) ) AS distance FROM wp_postmeta WHERE meta_key='location_options.google_map' HAVING distance <= 500 ORDER BY distance ASC");

                            foreach ($myrows as $row) {
                                $location = $wf->post($row->locid);
                                $locationclrs = array(); // these will be the suitable counsellors who offer the service

                                if ($location->post_status == 'publish') {

                                        /*if ($_GET["service"] != "") {
                                        foreach ($location->location_options as $loc) {
                                            $cslrs = $loc->location_counsellors;
                                            $cslrs = explode(", ", $cslrs);

                                            foreach ($cslrs as $cslr) {
                                                // output the counsellor
                                                $counsellor = $wf->post("".$cslr, "counsellor");
                                                $services = $counsellor->counsellor_profile->services_offered;
                                                //if (strpos($services, htmlspecialchars($_GET["service"])) !== false) {
                                                array_push($locationclrs, $counsellor);
                                                //}                                             
                                            }
                                        }
                                    } else { */
                                    array_push($locationclrs, '1'); // this is just to force true the next IF statement
                                    //}

                                    if (!empty($locationclrs)) {
                                        echo '<!-- '.$row->locid.' ('.$row->distance.') -->';
                                        if (!array_key_exists($row->locid, $locations)) {
                                            $locations[$row->locid] = array($row->distance, $wf->post($row->locid));
                                        } else {
                                            $ltemp = $locations[$row->locid];
                                            if ($row->distance < $ltemp[0]) {
                                                $locations[$row->locid] = array($row->distance, $wf->post($row->locid));
                                            }
                                        }
                                    }
                                }
                            }

                             $numshown = 0;
                             sort($locations);
                             foreach ($locations as $location) {
                                 if ($numshown <= 10) {
                                     echo '<h3>'.$location[1]->link.' ('.number_format($location[0], 2).' kms)</h3>';
                                     ++$numshown;
                                 }
                             }

                             if ($numshown == 0) {
                                 echo 'No results were found for your search. Please try repeating the search with a suburb instead of a postcode.';
                                //echo "It appears that you are searching in a different state than you have selected on the top right hand corner of our website. Please ensure that the state selected is the state you are searching in. Once you have done this please repeat your search to be connected to a psychologist or counsellor in your area.";    
                             }
                        ?>  
                    
                        
                    </section> <!-- end article section -->

                </article> <!-- end article -->


            </div> <!-- end #main -->

            <?php get_sidebar('service'); ?>

    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
