    <footer class="footer" role="contentinfo">
        <div class="top-footer-nav">
            <?php wp_nav_menu(array("menu"=>"Bottom Nav")); ?>
        </div>

        <div id="inner-footer" class="wrap clearfix">
            <div class="logos">
                <h3>Professional memberships:</h3>
                <!-- <img src="<?php bloginfo('template_url'); ?>/library/images/aca01.jpg" /> -->
                <img src="<?php bloginfo('template_url'); ?>/library/images/AHPRA-LOGO.png" />
                <img src="<?php bloginfo('template_url'); ?>/library/images/pacfa-logo.png" />                        
                <img src="<?php bloginfo('template_url'); ?>/library/images/AASW-logo.png" />
                <img src="<?php bloginfo('template_url'); ?>/library/images/aarc-logo1.png" />
                <img src="<?php bloginfo('template_url'); ?>/library/images/AAFT-logo.png" />
            </div>

            <nav role="navigation">
                <?php wp_nav_menu(array("menu"=>"Footer Nav")); ?>
            </nav>
            
            <div class="contact">
                <div class="left">
                    <p class="footer-flag">Phone <a style="text-decoration: none; color: inherit;" href="tel:1300735030"><?php echo do_shortcode("[phone]"); ?></a> 
                    <p>Monday to Friday 8am to 8pm<br />Saturdays and Sundays 9am - 5:30pm</p>  
                </div>
                
                <div class="right">
                    <p>Find us on:</p>
                    <p>
                        <a href="https://www.facebook.com/Life-Supports-Counselling-203584619994380/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a>
                        <a href="https://twitter.com/life_supports" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"></a>
                        <a href="https://www.youtube.com/channel/UCM564TMQ6KFopGdnkvBXtuw" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/you_tube.png"></a>
                    </p>
                    <div class="dontaddthis_horizontal_follow_toolbox"></div>
                </div>
            </div><!-- .contact -->
            
        </div> <!-- end #inner-footer -->
        
        <div class="location-choice">
            <span>Your Location: </span>
            <select name="pretty" tabindex="1" class="pretty dk">
                <option value="Australian Capital Territory" <?php if ($_COOKIE['region']=="Australian Capital Territory") echo "selected"; ?>>Australian Capital Territory</option>
                <option value="New South Wales" <?php if ($_COOKIE['region']=="New South Wales") echo "selected"; ?>>New South Wales</option>
                <option value="Northern Territory" <?php if ($_COOKIE['region']=="Northern Territory") echo "selected"; ?>>Northern Territory</option>
                <option value="Queensland" <?php if ($_COOKIE['region']=="Queensland") echo "selected"; ?>>Queensland</option>
                <option value="South Australia" <?php if ($_COOKIE['region']=="South Australia") echo "selected"; ?>>South Australia</option>
                <option value="Victoria" <?php if ($_COOKIE['region']=="Victoria") echo "selected"; ?>>Victoria</option>
                <option value="Western Australia" <?php if ($_COOKIE['region']=="Western Australia") echo "selected"; ?>>Western Australia</option>
            </select>
            <?php if (isset($_COOKIE["firsttime"]) && $_COOKIE["firsttime"] == "hello") : ?>
                <span class="loctip"><img src="<?php bloginfo('template_url'); ?>/images/close.png" />We have automatically set your location, you can change it here.</span>
            <?php endif; ?>
        </div>
        <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. Find us on <a target="_blank" href="https://plus.google.com/104723022972881722656" rel="publisher">Google+</a> | <a href="https://www.facebook.com/Life-Supports-Counselling-203584619994380/" target="_blank">Facebook</a> | <a target="_blank" href="https://twitter.com/life_supports">Twitter</a> | <a target="_blank" href="https://www.youtube.com/channel/UCM564TMQ6KFopGdnkvBXtuw">Youtube</a></p><!-- end copyright -->
    </footer> <!-- end footer -->
</div> <!-- end #container -->

<!-- all js scripts are loaded in library/bones.php -->
<?php wp_footer(); ?>

<script>
jQuery( document ).ready(function() {
var hello = jQuery('.location-choice .dk_label').text();
jQuery( '#input_1_8' ).val(hello);
console.log(hello);
});
</script>
<script>
jQuery( document ).ready(function() {
    jQuery( ".rebates-list" ).append("<a href='http://www.lifesupportscounselling.com.au/private-health/'><li title='Private Health Insurance' class='PrivateHealthInsurance'>Private Health Insurance</li></a>");
});
</script>

</body>
</html> <!-- end page. what a ride! -->
