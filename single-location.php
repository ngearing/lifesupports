<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">
        <div class="top-content">

            <div id="main" class="eightcol first clearfix" role="main">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                        
                        <p class="searchHead mobile-only">Search Results</p>
                        <section class="entry-content clearfix" itemprop="articleBody">
                            <div id="sidebar1" class="sidebar fourcol clearfix couns mobile-only">
                                <h4 class="widgettitle mob">Counsellors in <?php echo $wf->the->title; ?></h4>
                                <?php if ( is_active_sidebar( 'locationsidebar' ) ) : ?>
                                    <?php dynamic_sidebar( 'locationsidebar' ); ?>
                                <?php else : ?>
                                    <div class="alert alert-help">
                                        <p><?php _e("Please activate some Widgets.", "bonestheme");  ?></p>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <h1 class="entry-title single-title not-mobile"><?php if (get_field('alternate_page_title')) {
                                    the_field('alternate_page_title');  
                                } else {
                                    the_title();
                                } ?></h1>   
                                
                                
                                <?php if ($wf->the->location_options->count>1) { $locationtext = "locations"; } else { $locationtext = "location"; }
                            echo "<h2 class=''>We have ".$wf->the->location_options->count." ".$locationtext." in ".$wf->the->title.":</h2>";       
                            ?>              
                            
                            
                            <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
                            <?php $i=0; foreach ($wf->the->location_options as $location) : $i++; ?>
                                <div class="location-box">
                                    <div class="location-address">
                                        <?php echo $location->location_address; ?>
                                    </div>
                                    <?php echo $location->location_directions; ?>
                                    <?php 
                                        $map = $location->google_map;
                                        if ($map->lat != "") { ?>
                                            
                                            <script type="text/javascript">
                                                function initialize<?php echo $i; ?>() {
                                                    var latlng = new google.maps.LatLng(<?php echo $map->lat.",".$map->lng; ?>);
                                                    var settings = {
                                                        zoom: 15,
                                                        center: latlng,
                                                        mapTypeControl: true,
                                                        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                                                        navigationControl: true,
                                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                                };
                                                var map = new google.maps.Map(document.getElementById("map_canvas<?php echo $i; ?>"), settings);
                                                var companyPos = new google.maps.LatLng(<?php echo $map->lat.",".$map->lng; ?>);
                                                var companyMarker = new google.maps.Marker({
                                                      position: companyPos,
                                                      map: map,
                                                      title:"Some title"
                                                });
                                            }
                                        </script>
                                        <div id="map_canvas<?php echo $i; ?>" style="width:620px; height:385px"></div>
                                        <script type="text/javascript">initialize<?php echo $i; ?>();</script>
                                    <?php   
                                        } else {
                                    ?>
                                        <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo strip_tags($location->location_address); ?>&markers=size:mid%7c<?php echo strip_tags(str_replace("<br />","%20",$location->location_address)); ?>&zoom=15&size=600x300&sensor=false&maptype=roadmap" />
                                    <?php
                                        }
                                    ?>
                                </div>
                            <?php endforeach; ?>
                            <p class="not-mobile">Life Supports counsellors offer flexible appointment times, including after hours, and Medicare rebates are available for most services.</p>
                            <p class="not-mobile">Find out how <a href="https://www.lifesupportscounselling.com.au/life-supports-accreditation/">Life Supports Accreditation</a> ensures that each of our counsellors and psychologists are experts in their area of practice and are equipped to create the long term positive change you need.</p>
                            <p class="not-mobile">The following counselling services are offered in <?php echo $wf->the->title; ?>:</p>
                            
                            <?php
                            $servicesarray = array();
                            foreach ($wf->the->location_options as $locn) { 
                                // get all counsellors in this location
                                $cslrs = $locn->location_counsellors;
                                $cslrs = explode(", ", $cslrs);
                                foreach ($cslrs as $cslr) {
                                    // output the counsellor
                                    $counsellor = "";
                                    $counsellor = $wf->post("".$cslr, "counsellor");
                                    $services = explode(",", $counsellor->counsellor_profile->services_offered);
                                    foreach ($services as $key => $value) {
                                        array_push($servicesarray, $value);
                                    }                                                   
                                }
                            } 
                            $servicesarray = array_unique($servicesarray);
                            echo "<ul class='not-mobile'>";
                            foreach ($servicesarray as $srvc) {
                                echo "<li>".$srvc."</li>";
                            } 
                            
                            echo "</ul><p class='not-mobile'>To make an appointment in ".$wf->the->title." please call us today on  ".do_shortcode("[phone]").".</p>";
                            
                            ?>
                            
                            
                            <header class="article-header clearfix">
                                <h2 class="entry-title single-title mobile-only" itemprop="headline">Counselling services in <?php echo $wf->the->title; ?></h2>
                            </header> <!-- end article header -->
                            <p class="mobile-only"><strong>Our experienced and qualified counsellors in <?php echo $wf->the->title; ?> offer professional and confidential counselling services.</strong></p>
                            <p class="mobile-only">Life Supports counsellors offer flexible appointment times, including after hours, and Medicare rebates are available for most services.</p>
                            <p class="mobile-only">Find out how <a href="https://www.lifesupportscounselling.com.au/life-supports-accreditation/">Life Supports Accreditation</a> ensures that each of our counsellors and psychologists are experts in their area of practice and are equipped to create the long term positive change you need.</p>
                            <p class="mobile-only">The following counselling services are offered in <?php echo $wf->the->title; ?>:</p>
                            <?php
                            $servicesarray = array();
                            foreach ($wf->the->location_options as $locn) { 
                                // get all counsellors in this location
                                $cslrs = $locn->location_counsellors;
                                $cslrs = explode(", ", $cslrs);
                                foreach ($cslrs as $cslr) {
                                    // output the counsellor
                                    $counsellor = "";
                                    $counsellor = $wf->post("".$cslr, "counsellor");
                                    $services = explode(",", $counsellor->counsellor_profile->services_offered);
                                    foreach ($services as $key => $value) {
                                        array_push($servicesarray, $value);
                                    }                                                   
                                }
                            } 
                            $servicesarray = array_unique($servicesarray);
                            echo "<ul class='mobile-only'>";
                            foreach ($servicesarray as $srvc) {
                                echo "<li>".$srvc."</li>";
                            } 
                            if ($wf->the->location_options->count>1) { $locationtext = "locations"; } else { $locationtext = "location"; }
                            echo "</ul><p class='mobile-only'>To make an appointment in ".$wf->the->title." please call us today on  ".do_shortcode("[phone]").".</p>";
                            ?>
                            
                        </section> <!-- end article section -->



                    </article> <!-- end article -->

                <?php endwhile; ?>

                <?php else : ?>

                    <article id="post-not-found" class="hentry clearfix">
                            <header class="article-header">
                                <h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
                            </header>
                            <section class="entry-content">
                                <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
                            </section>
                            <footer class="article-footer">
                                    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
                            </footer>
                    </article>

                <?php endif; ?>

            </div> <!-- end #main -->

            <?php get_sidebar("location"); ?>
        
        </div>
    </div>
    <div id="second-content" class="clearfix" role="complementary">
        <?php $incposts = $wf->the->incoming("post_type=post"); ?>
        <div class="heading-wrap">
            <h2>Recent articles <?php if (count($incposts)) echo "by ".$wf->the->title; ?></h2>
        </div>
        <div class="bottom-content wrap">
            <div id="second" class="twelvecol clearfix" >
                
                <?php 
                    if (count($incposts)) {
                        foreach ($incposts as $the) { outputposts($the); }
                    } else {
                        foreach ($wf->type("post")->posts("orderby=post_date&posts_per_page=4") as $the) { outputposts($the); }
                    }
                ?>

            </div>
        </div>


    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer();
