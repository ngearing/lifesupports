<?php
/*
 * Template Name: Service Parent Page
 */
get_header();
?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">

            <div id="main" class="eightcol clearfix" role="main">


                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="article-header">
                        <?php
                        if (function_exists('yoast_breadcrumb')) {
                            yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                        } ?>

                        <h1 class="page-title" itemprop="headline">
                        <?php
                        $postfix = '';
                        if ($s_location != '') {
                            $postfix = ' in '.ucwords(str_replace('-', ' ', $s_location));
                        }
                        if ($s_the->page_options->overwrite_title != '') {
                            echo $s_the->page_options->overwrite_title.$postfix;
                        } else {
                            echo $s_the->title.$postfix;
                        }
                        ?>
                        </h1>

                    </header> <!-- end article header -->

                    <section class="entry-content clearfix" itemprop="articleBody">
                        <?php echo $s_the->content; ?>
                    </section> <!-- end article section -->

                </article> <!-- end article -->

            

            </div> <!-- end #main -->

            <?php get_sidebar('service'); ?>

    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
