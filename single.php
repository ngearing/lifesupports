<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>									

								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<div class="alignright">
										<div class="social">
											<div class="left">
												<p class="hd">Share</p><div class="addthis_sharing_toolbox"></div>
											</div>
											<div class="right">
												<p class="hd">Follow</p><div class="addthis_horizontal_follow_toolbox"></div>
											</div>
										</div>
									

									<?php if($wf->the->has_featured_image()) {
											echo "<a href='".$wf->the->permalink."' title='".$wf->the->title."' class=''>".$wf->the->thumbnail->resize("w=570&h=420")."</a>";
										} else {
											echo "<a href='".$wf->the->permalink."' title='".$wf->the->title."' class=''>".$wf->theme_image("default-thumb.jpg")->resize("w=570&h=420")."</a>";
										} ?>
									</div>
									<p class="byline vcard"><?php
										printf(__('in %4$s', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), $wf->the->post_options->counsellor->link, get_the_category_list(', '));
									?></p>
									<?php the_content(); ?>
									
									<footer class="article-footer">
										<?php the_tags('<p class="tags"><span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', '</p>'); ?>
									</footer> <!-- end article footer -->
									
									<a class="button blue" href="<?php echo esc_url( home_url('/contact/')); ?>">Ask us how we can help you</a>
									
									<?php // printf(__('Would you like to learn more about %4$s counselling?', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), $wf->the->post_options->counsellor->link, get_the_category_list(', ')); ?>
									
								</section> <!-- end article section -->

								<?php //comments_template(); ?>

							</article> <!-- end article -->

						<?php endwhile; ?>

						<?php else : ?>

							<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
									</footer>
							</article>

						<?php endif; ?>

					</div> <!-- end #main -->

					<?php //get_sidebar(); ?>

				</div> <!-- end #inner-content -->
				
				
				<div id="second-content" class="clearfix" role="complementary">
					<div class="heading-wrap">
						<h2>Recent articles</h2>
					</div>
					<div class="bottom-content wrap">
						<div id="second" class="twelvecol clearfix" >
							
							<?php foreach ($wf->type("post")->posts("order=desc&orderby=date&posts_per_page=4") as $the) : ?>
	
								<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
									<header class="article-header">
	
										<h3 class="page-title" itemprop="headline">
											<?php 
												if ($the->page_options->overwrite_title != "")
													echo "<a href='".$the->permalink."' title='".$the->page_options->overwrite_title."' class='alignleft'>".$the->page_options->overwrite_title."</a>";
												else
													echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$the->title."</a>";
											?>
										</h3>
				
									</header> <!-- end article header -->
	
									<section class="entry-content clearfix" itemprop="articleBody">
										<?php if($the->has_featured_image()) {
											echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$the->thumbnail->resize("w=150&h=150")."</a>";
										} else {
											echo "<a href='".$the->permalink."' title='".$the->title."' class='alignleft'>".$wf->theme_image("default-thumb.jpg")->resize("w=150&h=150")."</a>";
										} ?>
										<?php echo $the->excerpt; echo $the->link(array('text'=>"(read more)")); ?>
									</section> <!-- end article section -->
	
								</article> <!-- end article -->
							
							<?php endforeach; ?>
						</div>
					</div>

				</div> <!-- end #inner-content -->
				
				
				

			</div> <!-- end #content -->

<?php get_footer(); ?>
