<div id="sidebar1" class="sidebar fourcol clearfix" role="complementary">

<?php if (is_active_sidebar('counsellorsidebar')) : ?>

    <?php dynamic_sidebar('counsellorsidebar'); ?>
        
    <div class="widget widget_services_offered">  
    <?php
    $services = $wf->the->counsellor_profile->services_offered;
    if (!$services->blank) {
        echo '<h4 class="widgettitle">Areas of Expertise</h4><ul>';

        $services = explode(',', $services);
        foreach ($services as $key => $value) {
            echo "<li>$value</li>";
        }
        echo '</ul>';
    }
    ?>
    </div>

<?php else : ?>

    <!-- This content shows up if there are no widgets defined in the backend. -->
    <div class="alert alert-help">
        <p><?php _e('Please activate some Widgets.', 'bonestheme');  ?></p>
    </div>

<?php endif; ?>

</div> 