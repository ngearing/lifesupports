<?php
/*
 * Template Name: Phone Counsellors Page
 */
get_header();
$region = $_COOKIE["region"];
?>
<div id="content">
    <div id="inner-content" class="wrap clearfix">
        <div id="main" class="eightcol clearfix" role="main">

            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                <header class="article-header">
                    <h1 class="page-title" itemprop="headline">
                    <?php
                    if ($wf->the->page_options->overwrite_title != "") {
                        echo $wf->the->page_options->overwrite_title;
                    } else {
                        echo $wf->the->title;
                    }
                    ?>
                    </h1>
                </header> <!-- end article header -->
            
            <section class="entry-content clearfix" itemprop="articleBody">
                <?php
                $doneids = array();
                foreach ($wf->type("counsellor") as $counsellor) {
                    if ($counsellor->counsellor_options->appointments_by->data->val) :
                        foreach ($counsellor->counsellor_options->appointments_by->data->val as $key => $value) {
                            if ($value=="Phone") {
                                $bio = explode(".", $counsellor->counsellor_profile->counsellor_bio);
                                if ($counsellor->id != "") {
                                    if (!$doneids[$counsellor->ID]) {
                                        echo "<div class='counsellor-minibox clearfix'><p class='ctitle'><a href='".$counsellor->permalink."'>".$counsellor->thumbnail."</a><strong>".$counsellor->link("title=$counsellor->title")."</strong>".$bio[0].".</p></div>";
                                        $doneids[$counsellor->ID] = "true";
                                    }
                                }
                            }
                        }
                    endif;
                }
                ?>
            </section> <!-- end article section -->

            </article> <!-- end article -->

        </div> <!-- end #main -->
        <?php get_sidebar("service"); ?>
    </div> <!-- end #inner-content -->
</div> <!-- end #content -->

<?php
get_footer();
